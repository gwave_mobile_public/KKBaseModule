# KKBaseModule

[![CI Status](https://img.shields.io/travis/yangpeng/KKBaseModule.svg?style=flat)](https://travis-ci.org/yangpeng/KKBaseModule)
[![Version](https://img.shields.io/cocoapods/v/KKBaseModule.svg?style=flat)](https://cocoapods.org/pods/KKBaseModule)
[![License](https://img.shields.io/cocoapods/l/KKBaseModule.svg?style=flat)](https://cocoapods.org/pods/KKBaseModule)
[![Platform](https://img.shields.io/cocoapods/p/KKBaseModule.svg?style=flat)](https://cocoapods.org/pods/KKBaseModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KKBaseModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KKBaseModule'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

KKBaseModule is available under the MIT license. See the LICENSE file for more info.
