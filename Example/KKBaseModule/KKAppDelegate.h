//
//  KKAppDelegate.h
//  KKBaseModule
//
//  Created by yangpeng on 07/18/2022.
//  Copyright (c) 2022 yangpeng. All rights reserved.
//

@import UIKit;

@interface KKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
