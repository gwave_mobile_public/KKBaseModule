//
//  KKViewController.m
//  KKBaseModule
//
//  Created by yangpeng on 07/18/2022.
//  Copyright (c) 2022 yangpeng. All rights reserved.
//

#import "KKViewController.h"
#import "HttpRequestTool.h"

@interface KKViewController ()

@end

@implementation KKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [HttpRequestTool sendRequest:^(HttpRequestConfiguration * _Nullable configure) {
        configure.requestType = KKRequestTypeGET;
        configure.specialUrl = @"https://api.beta.dipbit.xyz/financing/products";
        configure.parameters = @{@"language":@"zh",@"limit":@"10"};
    } Success:^(id  _Nullable responseObject) {
        
    } Failure:^(NSError * _Nullable error) {
        
    }];
    
    [HttpRequestTool sendRequest:^(HttpRequestConfiguration * _Nullable configure) {
        configure.requestType = KKRequestTypeGET;
        configure.specialUrl = @"https://api.beta.dipbit.xyz/financing/products";
        configure.parameters = @{@"language":@"zh",@"limit":@"10"};
    } Success:^(id  _Nullable responseObject) {
        
    } Failure:^(NSError * _Nullable error) {
        
    } networkFailure:^{
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
