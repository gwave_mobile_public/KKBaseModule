//
//  HttpRequestTool.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#import <Foundation/Foundation.h>
#import "HttpRequestConfiguration.h"

#define HttpRequestBaseUrl @""

typedef void (^KKResponseConfigure)(HttpRequestConfiguration * _Nullable configure);

NS_ASSUME_NONNULL_BEGIN

@interface HttpRequestTool : NSObject

// 需要成功和失败回调
+ (NSURLSessionDataTask *)sendRequest:(KKResponseConfigure)configureBlock
                              Success:(KKLoadSuccessBlock)success
                              Failure:(KKLoadFailedBlock)failure;
// 需要成功和失败回调，无网络回调
+ (NSURLSessionDataTask *)sendRequest:(KKResponseConfigure)configureBlock
                              Success:(KKLoadSuccessBlock)success
                              Failure:(KKLoadFailedBlock)failure
                       networkFailure:(KKNetworkFailedBlock)failure;

@end

NS_ASSUME_NONNULL_END
