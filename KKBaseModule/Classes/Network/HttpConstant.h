//
//  HttpConstant.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#ifndef HttpConstant_h
#define HttpConstant_h
//#import <AFNetworking/AFNetworking.h>

// Basics网络请求成功回调
typedef void (^KKResponseSuccess)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject);
// Basics网络请求失败回调
typedef void (^KKResponseFailed)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error);
// Basics网络请求结束回调（无论成功或者失败）
typedef void (^KKResponseFinished)(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject, NSError * _Nullable error);

// Tool网络请求成功回调
typedef void (^KKLoadSuccessBlock)(id _Nullable responseObject);
// Tool网络请求失败回调
typedef void (^KKLoadFailedBlock)(NSError * _Nullable error);
// 网络不可用
typedef void (^KKNetworkFailedBlock)(void);

typedef enum : NSUInteger {
    KKRequestTypeGET,// GET请求
    KKRequestTypePOST,// POST请求
} KKRequestType;

typedef enum : NSUInteger {
    KKHttpNotReachable,// 网络不可用
    KKHttpReachableViaWiFi,// WiFi网络
    KKHttpReachableViaWWAN,// 可用网络
} KKHttpNetworkStatusType;

#endif /* HttpConstant_h */
