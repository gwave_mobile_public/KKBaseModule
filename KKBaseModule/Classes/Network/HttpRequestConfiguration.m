//
//  HttpRequestConfiguration.m
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#import "HttpRequestConfiguration.h"

@implementation HttpRequestConfiguration

+ (instancetype)setConfigure{
    return [[HttpRequestConfiguration alloc] init];
}
// -- 默认参数在这里设置 --
- (instancetype)init{
    self = [super init];
    if (self) {
        // 默认请求方式为GET
        _requestType = KKRequestTypeGET;
        // 默认请求超时时间为60秒
        _timeoutInterval = 60.0;
    }
    return self;
}

@end
