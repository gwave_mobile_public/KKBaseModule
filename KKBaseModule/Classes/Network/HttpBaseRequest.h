//
//  HttpBaseRequest.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#import <Foundation/Foundation.h>
#import "HttpRequestConfiguration.h"
#import "HttpConstant.h"

NS_ASSUME_NONNULL_BEGIN

@interface HttpBaseRequest : NSObject

// 网络请求
- (NSURLSessionDataTask *)requestWithConfigure:(HttpRequestConfiguration *)configure
                                       Success:(KKResponseSuccess)success
                                       Failure:(KKResponseFailed)failure;
// 实例化
+ (instancetype)defaultCenter;

@end

NS_ASSUME_NONNULL_END
