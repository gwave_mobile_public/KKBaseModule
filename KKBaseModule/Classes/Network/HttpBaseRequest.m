//
//  HttpBaseRequest.m
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#import "HttpBaseRequest.h"

@implementation HttpBaseRequest

+ (instancetype)defaultCenter {
    static id requestBasics = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requestBasics = [[HttpBaseRequest alloc] init];
    });
    return requestBasics;
}

//- (NSURLSessionDataTask *)requestWithConfigure:(HttpRequestConfiguration *)configure
//                                       Success:(KKResponseSuccess)success
//                                       Failure:(KKResponseFailed)failure {
//    AFHTTPSessionManager *sessionManger = [AFHTTPSessionManager manager];
//    // -- 网络超时时间设置 --
//    [sessionManger.requestSerializer willChangeValueForKey:@"timeoutInterval"];
//    sessionManger.requestSerializer.timeoutInterval = configure.timeoutInterval;
//    [sessionManger.requestSerializer didChangeValueForKey:@"timeoutInterval"];
//    sessionManger.requestSerializer = [AFHTTPRequestSerializer serializer];
//    sessionManger.responseSerializer = [AFHTTPResponseSerializer serializer];
//    sessionManger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
//    [sessionManger.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//
//    switch (configure.requestType) {
//        case KKRequestTypeGET:{
//            return [sessionManger GET:configure.requestUrl parameters:configure.parameters headers:configure.header progress:^(NSProgress * _Nonnull uploadProgress) {
//
//            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                success(task,responseObject);
//            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                failure(task,error);
//            }];
//            break;
//        }
//        case KKRequestTypePOST:{
//            return [sessionManger POST:configure.requestUrl parameters:configure.parameters headers:configure.header progress:^(NSProgress * _Nonnull uploadProgress) {
//
//            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                success(task,responseObject);
//            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                failure(task,error);
//            }];
//            break;
//        }
//    }
//}
@end
