//
//  HttpRequestTool.m
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/8.
//

#import "HttpRequestTool.h"
#import "HttpConstant.h"
#import "HttpBaseRequest.h"

@implementation HttpRequestTool

+ (instancetype)defaultCenter{
    static id requestTool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requestTool = [[HttpRequestTool alloc] init];
    });
    return requestTool;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self startMonitoring];
    }
    return self;
}

- (void)startMonitoring
{
//    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
//    [manager startMonitoring];
//    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        switch (status) {
//            case AFNetworkReachabilityStatusUnknown:
//                NSLog(@"未识别的网络");
//                break;
//
//            case AFNetworkReachabilityStatusNotReachable:
//                NSLog(@"不可达的网络(未连接)");
//                break;
//
//            case AFNetworkReachabilityStatusReachableViaWWAN:
//                NSLog(@"2G,3G,4G...的网络");
//                break;
//
//            case AFNetworkReachabilityStatusReachableViaWiFi:
//                NSLog(@"wifi的网络");
//                break;
//            default:
//                break;
//        }
//    }];
}

+ (BOOL)networkisAvaliable
{
//    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
//    if (status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusUnknown) {
//        return YES;
//    }
    return NO;
}


+ (NSURLSessionDataTask *)sendRequest:(KKResponseConfigure)configureBlock
                              Success:(KKLoadSuccessBlock)success
                              Failure:(KKLoadFailedBlock)failure{
    return [[HttpRequestTool defaultCenter] sendRequest:configureBlock
                                                Success:success
                                                Failure:failure networkFailure:nil];
}

// 需要成功和失败回调，无网络回调
+ (NSURLSessionDataTask *)sendRequest:(KKResponseConfigure)configureBlock
                              Success:(KKLoadSuccessBlock)success
                              Failure:(KKLoadFailedBlock)failure
                       networkFailure:(KKNetworkFailedBlock)networkFailure
{
    return [[HttpRequestTool defaultCenter] sendRequest:configureBlock
                                                Success:success
                                                Failure:failure networkFailure:networkFailure];
}

#pragma mark 私有方法，外部进来的方法统一调用这个方法进行网络请求
- (NSURLSessionDataTask *)sendRequest:(KKResponseConfigure)configureBlock
                              Success:(KKLoadSuccessBlock)success
                              Failure:(KKLoadFailedBlock)failure
                       networkFailure:(KKNetworkFailedBlock)networkFailure{
    HttpRequestConfiguration *configure = [HttpRequestConfiguration setConfigure];
    if (configureBlock) {
        configureBlock(configure);
    }
    HttpRequestConfiguration *requestConfigure = [self resetRequestConfiguration:configure];
    if ([HttpRequestTool networkisAvaliable]) {
        return [[HttpBaseRequest defaultCenter] requestWithConfigure:requestConfigure Success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *data = [self analyzeResponseData:responseObject];
            if (success) {
                success(data);
            }
        } Failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(error);
            }
        }];
    } else {
        if (networkFailure) {
            networkFailure();
        }
    }
    return nil;
}

#pragma mark 重置请求配置 -- 按后台要去设置请求头和请求体，自行修改 --
- (HttpRequestConfiguration *)resetRequestConfiguration:(HttpRequestConfiguration *)configure{
    // 请求地址
    if (configure.specialUrl.length == 0) {
        [configure setValue:[NSString stringWithFormat:@"%@%@",HttpRequestBaseUrl,configure.apiUrl] forKey:@"requestUrl"];
    }
    else{
        [configure setValue:configure.specialUrl forKey:@"requestUrl"];
    }
    
    //设置请求header
    NSMutableDictionary *headerDict = [[NSMutableDictionary alloc] init];
    [headerDict setValue:@"application/json" forKey:@"Content-Type"];
    [headerDict setValue:@"application/json;charset=utf-8" forKey:@"Accept"];
    [headerDict setValue:@"kiki" forKey:@"saas_id"];
    [headerDict setValue:@"757C9B45-E172-4C27-ABEA-DAC747F7A380" forKey:@"deviceId"];
    [headerDict setValue:@"3021969adc0b0c1b5e80530285cb92e0" forKey:@"traceId"];
    configure.header = headerDict;;
    
    //设置请求body
    if (configure.parameters) {
        NSDictionary *body = configure.parameters;
        NSData *BodyData = [NSJSONSerialization dataWithJSONObject:body options:NSJSONWritingPrettyPrinted error:nil];
        NSString *bodyString = [[NSString alloc]initWithData:BodyData encoding:NSUTF8StringEncoding];
        configure.parameters = @{@"body":bodyString};
    }
    return configure;
}
#pragma mark 解析返回的数据 -- 这部分看接口返回的数据情况而定，自行修改 --
- (NSDictionary *)analyzeResponseData:(id)responseObject{
    if (responseObject == nil || [responseObject isEqual:[NSNull null]]) {
        return @{};
    }
    NSDictionary *paramDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
    return paramDict;
}

@end
