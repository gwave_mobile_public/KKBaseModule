//
//  KKNavigationController.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KKNavigationController : UINavigationController<UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property(nonatomic,weak) UIViewController* currentShowVC;
@property (nonatomic, assign) BOOL isPopGesture;// YES：打开侧滑返回

@end

NS_ASSUME_NONNULL_END
