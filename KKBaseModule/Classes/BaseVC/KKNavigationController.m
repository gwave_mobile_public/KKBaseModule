//
//  KKNavigationController.m
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/7/26.
//

#import "KKNavigationController.h"

@interface KKNavigationController ()

@end

@implementation KKNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    if (self = [super initWithRootViewController:rootViewController]) {
        self.interactivePopGestureRecognizer.delegate = self;
        self.delegate = self;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    if (@available(iOS 13.0, *)) {
        UINavigationBarAppearance *appearance = [UINavigationBarAppearance new];
        [appearance configureWithDefaultBackground];
        appearance.backgroundColor =  [UIColor whiteColor];
        appearance.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
        appearance.shadowColor = [UIColor clearColor];//导航栏底部线不显示
        self.navigationBar.barTintColor = [UIColor blackColor];
        self.navigationBar.standardAppearance = appearance;
        self.navigationBar.scrollEdgeAppearance= appearance;
    }
    else {
        self.navigationBar.barTintColor =  [UIColor blackColor];
        [[UINavigationBar appearance] setTranslucent:NO];
    }
    self.delegate = self;
    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
}

//- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    //push的时候隐藏底部tabbar
//    if(self.viewControllers.count != 0){
//        viewController.hidesBottomBarWhenPushed = YES;
//        self.tabBarController.tabBar.hidden = YES;
//    }
//    [super pushViewController:viewController animated:animated];
//}
//
//// fix: https://developer.apple.com/forums/thread/660750
//- (NSArray<__kindof UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated
//{
//    if (@available(iOS 14.0, *)) {
//        for (UIViewController *vc in self.viewControllers) {
//            vc.hidesBottomBarWhenPushed = NO;
//            self.tabBarController.tabBar.hidden = NO;
//        }
//    }
//    return [super popToRootViewControllerAnimated:animated];
//}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (navigationController.viewControllers.count == 1){
        //如果堆栈内的视图控制器数量为1 说明只有根控制器，将currentShowVC 清空，为了下面的方法禁用侧滑手势
        self.currentShowVC = Nil;
    }
    else{
        //将push进来的视图控制器赋值给currentShowVC
        self.currentShowVC = viewController;
    }

    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        if (self.viewControllers.count == 1) {// 禁止首页的侧滑返回
            navigationController.interactivePopGestureRecognizer.enabled = NO;
        }else{
            navigationController.interactivePopGestureRecognizer.enabled = YES;
        }
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (!self.isPopGesture) {
        return NO;
    }
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        if (self.currentShowVC == self.topViewController) {
            //如果 currentShowVC 存在说明堆栈内的控制器数量大于 1 ，允许激活侧滑手势
            return YES;
        }
        return NO;
    }
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
