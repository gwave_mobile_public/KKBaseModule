//
//  UIFont+Addition.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (Addition)

+ (UIFont *)kk_MediumFont:(CGFloat)fontSize;
+ (UIFont *)kk_RegularFont:(CGFloat)fontSize;
+ (UIFont *)kk_SemiboldFont:(CGFloat)fontSize;
+ (UIFont *)kk_LightFont:(CGFloat)fontSize;

@end

NS_ASSUME_NONNULL_END
