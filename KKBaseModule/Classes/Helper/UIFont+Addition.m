//
//  UIFont+Addition.m
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/8/2.
//

#import "UIFont+Addition.h"

@implementation UIFont (Addition)

+ (UIFont *)kk_RegularFont:(CGFloat)fontSize{
    
    UIFont * font = [UIFont fontWithName:@"PingFangSC-Regular" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)kk_MediumFont:(CGFloat)fontSize{
    
    UIFont * font = [UIFont fontWithName:@"PingFangSC-Medium" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)kk_SemiboldFont:(CGFloat)fontSize{
    
    UIFont * font = [UIFont fontWithName:@"PingFangSC-Semibold" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

+ (UIFont *)kk_LightFont:(CGFloat)fontSize{
    
    UIFont * font = [UIFont fontWithName:@"PingFangSC-Light" size:fontSize];
    if (font == nil) {
        font = [UIFont systemFontOfSize:fontSize];
    }
    return font;
}

@end
