//
//  define.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/7/18.
//

#ifndef define_h
#define define_h


#endif /* define_h */

// frame
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define IS_IPHONE_4 (CGSizeEqualToSize(CGSizeMake(320, 480), [[UIScreen mainScreen] bounds].size))
#define IS_IPHONE_5 (CGSizeEqualToSize(CGSizeMake(320, 568), [[UIScreen mainScreen] bounds].size))
#define IS_IPHONE_6 (CGSizeEqualToSize(CGSizeMake(375, 667), [[UIScreen mainScreen] bounds].size))
#define IS_IPHONE_PLUS (CGSizeEqualToSize(CGSizeMake(414, 736), [[UIScreen mainScreen] bounds].size))
//iPhone X   iPhone XS iPhone 12 mini
#define IS_IPHONE_XS (CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size))
//iPhone XS Max   iPhone XR
#define IS_IPHONE_XR (CGSizeEqualToSize(CGSizeMake(414, 896), [[UIScreen mainScreen] bounds].size))
//iPhone 12
#define IS_IPHONE_12 (CGSizeEqualToSize(CGSizeMake(390, 844), [[UIScreen mainScreen] bounds].size))
//iPhone 12 pro max
#define IS_IPHONE_12_PROMAX (CGSizeEqualToSize(CGSizeMake(428, 926), [[UIScreen mainScreen] bounds].size))
//代表X系列的机型
#define IS_IPHONE_X (IS_IPHONE_XS || IS_IPHONE_XR || IS_IPHONE_12 || IS_IPHONE_12_PROMAX)

#define STATUSBAR_HEIGHT (IS_IPHONE_X?UIApplication.sharedApplication.delegate.window.safeAreaInsets.top:20)
#define NAVIGATIONBAR_HEIGHT (STATUSBAR_HEIGHT + 44)
#define TABBAR_HEIGHT (IS_IPHONE_X?83:49)
#define TABBAR_SPACINGBOTTOM (IS_IPHONE_X?34:0)


#define WEAKSELF __weak typeof(self) weakSelf = self;
#define STRONGSELF __strong typeof(weakSelf) strongSelf = weakSelf;

