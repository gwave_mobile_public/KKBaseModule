//
//  KKDateHelp.h
//  KKBaseModule
//
//  Created by 杨鹏 on 2022/9/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KKDateHelp : NSObject

// get hour minute eg:12:34
+ (NSString *)getHourMinuteFromTimestamp:(NSString *)timestamp;
// get month day eg:03/20
+ (NSString *)getMonthDayFromTimestamp:(NSString *)timestamp;
// get year eg:2021
+ (NSString *)getYearFromTimestamp:(NSString *)timestamp;

// get time eg:yyyy MM-dd HH
+ (NSString *)getYearToHourFromTimestamp:(NSString *)timestamp;
// get time eg:yyyy MM
+ (NSString *)getYearMonthFromTimestamp:(NSString *)timestamp;
//get time eg: yyyy MM/dd
+ (NSString *)getYearMonthDayFromTimestamp:(NSString *)timestamp;

// get time eg:02-22 09:30
+ (NSString *)getTimeFromTimestamp:(NSString *)timestamp;

//get time eg:yyyy-MM-dd HH:mm
+ (NSString *)getYearToMinuteToSecondFromTimestamp:(NSString *)timestamp;

//get time eg:yyyy MM-dd HH:mm
+ (NSString *)getYearToMinuteFromTimestamp:(NSString *)timestamp;

// get time eg:09:30
+ (NSString *)getHourTimeFromTimestamp:(NSString *)timestamp;

// formatterString 具体格式
+ (NSString *)getFormatteredString:(NSString *)formatterString timestamp:(NSString *)timestamp;

// check if same minute
+ (BOOL)isSameMinuteInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval;

// check if same hour
+ (BOOL)isSameHourInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval;

// check if same day
+ (BOOL)isSameDay:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same week
+ (BOOL)isSameWeek:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same month
+ (BOOL)isSameMonth:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same year
+ (BOOL)isSameYear:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;
@end

NS_ASSUME_NONNULL_END
